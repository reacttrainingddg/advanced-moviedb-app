const defaultState = {
  Data: {
    Movies: {
      moviesById: {
        '10191': {
          adult: false,
          backdrop_path: 'https://image.tmdb.org/t/p/original/dEhTNJ7RoFC5C72jFVXoVLYBeb4.jpg',
          belongs_to_collection: {
            id: 89137,
            name: 'How to Train Your Dragon Collection',
            poster_path: '/4tBKIkPLFMkiZETjAMOHNoty8B1.jpg',
            backdrop_path: '/mvcfPkOvgDJG2lEAxTz0NKqoQLo.jpg'
          },
          budget: 165000000,
          genres: [
            {
              id: 14,
              name: 'Fantasy'
            },
            {
              id: 12,
              name: 'Adventure'
            },
            {
              id: 16,
              name: 'Animation'
            },
            {
              id: 10751,
              name: 'Family'
            }
          ],
          homepage: 'http://www.howtotrainyourdragon.com/',
          id: 10191,
          imdb_id: 'tt0892769',
          original_language: 'en',
          original_title: 'How to Train Your Dragon',
          overview: 'As the son of a Viking leader on the cusp of manhood, shy Hiccup Horrendous Haddock III faces a rite of passage: he must kill a dragon to prove his warrior mettle. But after downing a feared dragon, he realizes that he no longer wants to destroy it, and instead befriends the beast – which he names Toothless – much to the chagrin of his warrior father',
          popularity: 40.186,
          poster_path: 'https://image.tmdb.org/t/p/w300/hIXX3IRFy0InUOmYeWjvhCAgQNj.jpg',
          production_companies: [
            {
              id: 521,
              logo_path: '/kP7t6RwGz2AvvTkvnI1uteEwHet.png',
              name: 'DreamWorks Animation',
              origin_country: 'US'
            }
          ],
          production_countries: [
            {
              iso_3166_1: 'US',
              name: 'United States of America'
            }
          ],
          release_date: '2010-03-10',
          revenue: 494878759,
          runtime: 100,
          spoken_languages: [
            {
              iso_639_1: 'en',
              name: 'English'
            }
          ],
          status: 'Released',
          tagline: 'One adventure will change two worlds',
          title: 'How to Train Your Dragon',
          video: false,
          vote_average: 7.7,
          vote_count: 7247
        },
        '157336': {
          adult: false,
          backdrop_path: 'https://image.tmdb.org/t/p/original/xu9zaAevzQ5nnrsXN6JcahLnG4i.jpg',
          belongs_to_collection: null,
          budget: 165000000,
          genres: [
            {
              id: 12,
              name: 'Adventure'
            },
            {
              id: 18,
              name: 'Drama'
            },
            {
              id: 878,
              name: 'Science Fiction'
            }
          ],
          homepage: 'http://www.interstellarmovie.net/',
          id: 157336,
          imdb_id: 'tt0816692',
          original_language: 'en',
          original_title: 'Interstellar',
          overview: 'Interstellar chronicles the adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.',
          popularity: 46.793,
          poster_path: 'https://image.tmdb.org/t/p/w300/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg',
          production_companies: [
            {
              id: 923,
              logo_path: '/5UQsZrfbfG2dYJbx8DxfoTr2Bvu.png',
              name: 'Legendary Entertainment',
              origin_country: 'US'
            },
            {
              id: 9996,
              logo_path: '/3tvBqYsBhxWeHlu62SIJ1el93O7.png',
              name: 'Syncopy',
              origin_country: 'GB'
            },
            {
              id: 13769,
              logo_path: null,
              name: 'Lynda Obst Productions',
              origin_country: ''
            }
          ],
          production_countries: [
            {
              iso_3166_1: 'GB',
              name: 'United Kingdom'
            },
            {
              iso_3166_1: 'US',
              name: 'United States of America'
            }
          ],
          release_date: '2014-11-05',
          revenue: 675120017,
          runtime: 169,
          spoken_languages: [
            {
              iso_639_1: 'en',
              name: 'English'
            }
          ],
          status: 'Released',
          tagline: 'Mankind was born on Earth. It was never meant to die here.',
          title: 'Interstellar',
          video: false,
          vote_average: 8.2,
          vote_count: 17839
        }
      },
      allMovieIds: [
        157336,
        10191
      ],
      currentMovieId: 10191,
      searchResults: []
    },
    Favorites: {
      favoritesIds: [
        157336,
        10191
      ]
    }
  },
  Search: {
    isSearching: false
  }
};
