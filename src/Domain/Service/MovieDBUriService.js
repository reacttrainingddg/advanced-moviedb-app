const baseUrl = 'https://api.themoviedb.org/3';
const apiKey = 'api_key=1a8ee4f652c883843501e842507cf1ca';
const buildMovieDBUri = (url) => ({
  url: ''.concat(baseUrl, url, url.includes('?') ? '&' : '?', apiKey),
  method: 'get',
  crossDomain: true,
  async: true,
  responseType: 'json'
});

export default buildMovieDBUri;
