import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import classes from './SearchBar.module.scss';
import {selectors, actions} from '../../Redux/Store';

const mapStateToProps = (state) => ({
  searchResults: selectors.Data.Movies.searchResults(state),
  isSearching: selectors.Search.isSearching(state),
});

const mapDispatchToProps = (dispatch) => ({
  onSearchButtonClick: (searchText) => {
    dispatch(actions.Search.execute(searchText));
  },
  onMovieSelect: (movieId) => {
    dispatch(actions.Data.Movies.fetchMovieData(movieId));
  },
  setCurrentMovie: (movieId) => {
    dispatch(actions.Data.Movies.setCurrentMovie(movieId));
  }
});

class SearchBar extends React.PureComponent {
  static propTypes = {
    // mapStateToProps
    // TODO
    searchResults: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
    })).isRequired,
    isSearching: PropTypes.bool.isRequired,

    // mapDispatchToProps
    onSearchButtonClick: PropTypes.func.isRequired,
    onMovieSelect: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.searchFieldRef = React.createRef();
  }

  handleSearchForm = (event) => {
    const searchText = this.searchFieldRef.current.value;
    if (searchText.length > 0) {
      this.props.onSearchButtonClick(searchText);
    }
    event.preventDefault();
    event.stopPropagation();
    return false;
  };

  handleSearchResultClick = (movieId) => {
    this.props.onMovieSelect(movieId);
    this.props.setCurrentMovie(movieId);
  };

  render() {
    return (
      <div className={classes["SearchBar"]}>
        <div className={classes["SearchBar__Bar"]}>
          <form className={classes["SearchBar__Form"]} action="#" onSubmit={this.handleSearchForm}>
            <input className={classes["SearchBar__FormInput"]} ref={this.searchFieldRef} type="text" placeholder="Enter a movie title..." />
            <button className={classes["SearchBar__FormSubmit"]} type="submit">Find Movies</button>
            {this.props.isSearching ?
              <svg className={classes["SearchBar__FormSpinner"]} width="48px" height="48px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
                   preserveAspectRatio="xMidYMid">
                <circle cx="50" cy="50" fill="none" stroke="#01d277"
                        strokeWidth="10" r="24" strokeDasharray="113.09733552923255 39.69911184307752"
                        transform="rotate(17.6393 50 50)">
                  <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50"
                                    keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform>
                </circle>
              </svg>
              : null}
          </form>
        </div>
        { this.props.searchResults.length > 0 ?
          <div className={classes["SearchBar__Results"]}>
            {this.props.searchResults.map(item =>
            <div key={item.id} className={classes["SearchBar__ResultsItem"]}>
              <h2 onClick={() => {this.handleSearchResultClick(item.id)}}>{item.title}</h2>
            </div>)}
          </div>
        : null}
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);
