import React from 'react';
import PropTypes from 'prop-types';
import classes from './ErrorMessage.module.scss';
import { connect } from 'react-redux';
import { actions } from '../../Redux/Store';

const mapStateToProps = (state) => ({
  message: state.Search.errorMessage,
});

const mapDispatchToProps = (dispatch) => ({
  hide: () => dispatch(actions.Search.setError('')),
});

class ErrorMessage extends React.PureComponent {
  static propTypes = {
    message: PropTypes.string.isRequired,
  };

  render() {
    return this.props.message && this.props.message.length > 0 ? (
      <div className={classes.ErrorMessage}>
        <h2>{this.props.message}</h2>
        <button onClick={this.props.hide}>OK</button>
      </div>
    ) : null;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ErrorMessage);
