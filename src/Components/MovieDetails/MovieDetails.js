import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import MoviePoster from '../MoviePoster';
import classes from './MovieDetails.module.scss';
import { selectors, actions } from '../../Redux/Store';

const mapStateToProps = (state) => {
  const movie = selectors.Data.Movies.currentMovie(state);
  return {
    movie,
    isMovieFavorite: selectors.Data.Favorites.isMovieFavorite(state, movie),
  };
};

const mapDispatchToProps = {
  addFavorite: actions.Data.Favorites.addFavorite,
  removeFavorite: actions.Data.Favorites.removeFavorite,
}

const defaultBackDropPath = 'https://via.placeholder.com/1920x1080.jpeg/181A1F?text=No+Splash+Image+Found';

export class MovieDetails extends React.PureComponent {
  static propTypes = {
    // TODO: move movie 'type' declaration into own file in src/Domain
    movie: PropTypes.shape({
      title: PropTypes.string,
      tagline: PropTypes.string,
      overview: PropTypes.string,
      release_date: PropTypes.string, // ISO 'yyyy-mm-dd'
      backdrop_path: PropTypes.string,
      poster_path: PropTypes.string,
    }),
    isMovieFavorite: PropTypes.bool.isRequired,
    addFavorite: PropTypes.func,
    removeFavorite: PropTypes.func,
  };

  handleFavoriteButtonClick = () => {
    const { movie, isMovieFavorite, addFavorite, removeFavorite } = this.props;
    if (isMovieFavorite) {
      removeFavorite(movie.id);
    } else {
      addFavorite(movie.id);
    }
  }

  render() {
    const { movie, isMovieFavorite } = this.props;
    if (movie !== undefined) {
      return (
        <div className={classes.MovieDetails} style={{ backgroundImage: `url(${movie.backdrop_path || defaultBackDropPath})` }}>
          <div className={classes.MovieDetails__grid} style={{ backgroundImage: `url(${movie.backdrop_path || defaultBackDropPath})` }}>
            <div className={classes.MovieDetails__blurredBackground}></div>
            <h2 className={classes.MovieDetails__header}>
              <span className={classes.header__title}>{movie.title}</span>
              {movie.release_date ? (
                <span className={classes.header__year}>{`(${new Date(movie.release_date).getFullYear()})`}</span>
              ) : null}
            </h2>
            <span className={classes.MovieDetails__tagline}>{movie.tagline ? movie.tagline : ''}</span>
            <div className={classes.MovieDetails__poster}><MoviePoster src={movie.poster_path} /></div>
            <div className={classes.MovieDetails__content}>
                <button
                  className={classes['MovieDetails__favorite-button']}
                  onClick={this.handleFavoriteButtonClick}
                >
                  {isMovieFavorite ? 'Remove From Favorites' : 'Add To Favorites'}
                </button>
                <h3>Overview</h3>
                <p>{movie.overview}</p>
            </div>
          </div>
        </div>
      );
    } else {
      return <h2 className={classes.MovieDetails__header}>'Loading..'</h2>;
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieDetails);
