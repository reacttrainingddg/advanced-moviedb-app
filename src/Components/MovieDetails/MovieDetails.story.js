import React from 'react';
import { storiesOf } from '@storybook/react';
import { MovieDetails } from './MovieDetails';

const movie = {
  id: 550,
  title: "Fight Club",
  tagline: "Mischief. Mayhem. Soap.",
  overview: "A ticking-time-bomb insomniac and a slippery soap salesman channel primal male aggression into a shocking new form of therapy. Their concept catches on, with underground \"fight clubs\" forming in every town, until an eccentric gets in the way and ignites an out-of-control spiral toward oblivion.",
  release_date: "1999-10-15",
  backdrop_path: "https://image.tmdb.org/t/p/original/52AfXWuXCHn3UjD17rBruA9f5qb.jpg",
  poster_path: "https://image.tmdb.org/t/p/original/adw6Lq9FiC9zjYEpOqfq03ituwp.jpg",
};

storiesOf('MovieDetails')
	.add('with default and favorite false', () => <MovieDetails
    movie={movie}
    isMovieFavorite={false}
    addFavorite={() => {}}
    removeFavorite={() => {}}
  />)
	.add('with default and favorite true', () => <MovieDetails
    movie={movie}
    isMovieFavorite={true}
    addFavorite={() => {}}
    removeFavorite={() => {}}
  />);
