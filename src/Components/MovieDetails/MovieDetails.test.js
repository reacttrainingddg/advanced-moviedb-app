import React from 'react';
import renderer from 'react-test-renderer';
import { MovieDetails }from './MovieDetails';

describe("Snapshot testing of a container", () => {
  it("renders same as before", () => {
    expect(renderer.create(<MovieDetails isMovieFavorite={false}/>)).toMatchSnapshot();
  });
});
