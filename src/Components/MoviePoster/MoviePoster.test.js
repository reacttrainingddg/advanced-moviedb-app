import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import MoviePoster from './MoviePoster';


describe("Basic assertions", () => {
  it("adds two numbers", () => {
    expect(1 + 2).toBe(3);
  });

  it("compares the VALUES of two objects", () => {
    expect({foo: "bar"}).toEqual({foo: "bar"});
  });

  it("compares the IDENTITY of two objects", () => {

    // This will fail
    // expect({foo: "bar"}).toBe({foo: "bar"});

    const baz = {foo: "bar"};
    expect(baz).toBe(baz);
  });

  it('adds floating point numbers correctly', () => {
    const value = 0.1 + 0.2;
    // This won't work because of rounding error
    // expect(value).toBe(0.3);
    expect(value).toBeCloseTo(0.3);
    expect(value).not.toBeCloseTo(0.35);
  });

  it('resolves to foo', () => {
    // make sure to add a return statement, otherwise Jest will crash
    return expect(Promise.resolve('foo')).resolves.toBe('foo');
  });
});


describe("Basic Rendering Tests", () => {

  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<MoviePoster />, div);
  });

});



describe("Snapshot testing", () => {
  it("adds two numbers", () => {
    // G E N I U S ! ! ! ! !
    expect(1 + 3).toMatchSnapshot();
  });

  it("renders same as before", () => {
    expect(renderer.create(<MoviePoster />)).toMatchSnapshot();
  });
});
