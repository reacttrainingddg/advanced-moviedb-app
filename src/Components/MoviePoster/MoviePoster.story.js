import React from 'react';
import { storiesOf } from '@storybook/react';
import MoviePoster from './MoviePoster';

storiesOf('MoviePoster', module)
	.add('with poster', () => <MoviePoster src='https://image.tmdb.org/t/p/original/adw6Lq9FiC9zjYEpOqfq03ituwp.jpg'/>)
	.add('with placeholder', () => <MoviePoster />);
