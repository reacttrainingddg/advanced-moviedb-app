import React from 'react';
import PropTypes from 'prop-types';

import classes from './MoviePoster.module.scss';

class MoviePoster extends React.PureComponent {
  static propTypes = {
    src: PropTypes.string.isRequired,
  }

  static defaultProps = {
    src: 'https://via.placeholder.com/350x500.jpeg?text=No+Poster',
  }

  render() {
      return (
        <img
          className={classes.MoviePoster}
          src={this.props.src}
          alt="movie poster"
        />
      );
  }
}

export default MoviePoster;
