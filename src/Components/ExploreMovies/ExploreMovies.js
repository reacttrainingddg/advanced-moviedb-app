import React from 'react';

import SearchBar from '../SearchBar';
import MovieDetails from '../MovieDetails';

export default class ExploreMovies extends React.PureComponent {
  render() {
    return (
      <>
        <SearchBar />
        <MovieDetails />
      </>
    );
  }
}
