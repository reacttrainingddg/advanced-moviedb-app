import React from 'react';

import FavoritesList from '../FavoritesList/FavoritesList';
import classes from './Favorites.module.scss';

export default class Favorites extends React.PureComponent {
  render() {
    return (
      <>
        <h2 className={classes.Favorites__title}>Favorites</h2>
        {
          // WORKSHOP TASK: sort by
          // WORKSHOP TASK: filter by
        }
        <FavoritesList />
      </>
    );
  }
}
