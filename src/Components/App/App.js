import React, { Component } from 'react';
import createClassName from 'classnames';
import { Route, Link, NavLink } from 'react-router-dom';

import ExploreMovies from '../ExploreMovies/ExploreMovies';
import Favorites from '../Favorites';
import logo from './logo.svg';
import classes from './App.module.scss';
import ErrorMessage from "../ErrorMessage";

class App extends Component {
  render() {
    return (
      <div className={classes.App}>
        <header className={classes["App-header"]}>
          <img src={logo} className={createClassName(classes["App-logo"], classes["App-logo--spinning"])} alt="logo" />
          <h1 className={classes["App-header__title"]}>
            <Link className={classes["App-header__title"]} to="/">Movie DB</Link>
          </h1>
          <div className={classes["App-header__nav"]}>
            <NavLink to="/" exact className={classes["App-header__link"]} activeClassName={classes["App-header__link--active"]}>Home</NavLink>
            <NavLink to="/favorites" className={classes["App-header__link"]} activeClassName={classes["App-header__link--active"]}>Favorites</NavLink>
          </div>
          <a href="https://www.themoviedb.org/" rel="noopener noreferrer" target="_blank">
            <img
              className={classes["App-logo"]}
              alt="powered by TMDB"
              src="https://www.themoviedb.org/assets/2/v4/logos/powered-by-rectangle-green-dcada16968ed648d5eb3b36bbcfdd8cdf804f723dcca775c8f2bf4cea025aad6.svg"
            />
          </a>
        </header>
        <main className={classes['App-main']}>
            <Route exact path="/" component={ExploreMovies} />
            <Route path="/favorites" component={Favorites} />
        </main>
        <ErrorMessage />
      </div>
    );
  }
}

export default App;
