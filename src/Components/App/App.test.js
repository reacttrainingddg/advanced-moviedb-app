import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import { createMockStore } from 'redux-test-utils';
import App from './App';

configure({ adapter: new Adapter() });

const shallowWithStore = (component, store) => {
  const context = {
    store,
  };
  return shallow(component, { context });
};

const store = createMockStore({});

describe("test the output without testing real markup", () => {
  it("renders a searchbar and movie details", () => {

    const wrapper = shallowWithStore(<App />, store);

    expect(wrapper.find('header').length).toEqual(1);
    expect(wrapper.find('main').length).toEqual(1);
    expect(wrapper.find('NavLink').length).toEqual(2);
    expect(wrapper.find('Route').length).toEqual(2);
  });
});
