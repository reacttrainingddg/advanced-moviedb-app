import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'


import {selectors, actions} from '../../Redux/Store';
import classes from './FavoriteListEntry.module.scss';

/**
 * Note: Performance wise it's not neccessary connect
 * each list element to the store. It's meant as an example.
 * Use this pattern for items in a List that change over time.
 * This is useful to prevent the whole list from rendering
 * when just a single element updates.
 */
const mapStateToProps = (state, props) => ({
  movie: selectors.Data.Movies.movieById(state, props.id)
});

const mapDispatchToProps = {
  removeFavorite: actions.Data.Favorites.removeFavorite,
  setCurrentMovie: actions.Data.Movies.setCurrentMovie,
};

class FavoriteListEntry extends React.PureComponent {
  static defaultProps = {
    id: PropTypes.number,
    movie: PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      tagline: PropTypes.string,
      overview: PropTypes.string,
      release_date: PropTypes.string, // ISO 'yyyy-mm-dd'
      backdrop_path: PropTypes.string,
      poster_path: PropTypes.string,
    }),
    removeFavorite: PropTypes.func,
    // react-router history from 'withRouter': https://reacttraining.com/react-router/web/api/history
    history: PropTypes.any,
  };

  // TODO: onClick -> route to index and set current movie to movie.id
  handleClick = () => {
    this.props.setCurrentMovie(this.props.movie.id);
    this.props.history.push('/');
  }

  handleRemove = () => {
    this.props.removeFavorite(this.props.id);
  }

  render() {
    const { movie } = this.props;

    return (
      <li className={classes.FavoriteListEntry}>
        <img className={classes.FavoriteListEntry__poster} alt="small poster" src={movie.poster_path} />
        <div className={classes.FavoriteListEntry__info} onClick={this.handleClick} role="button">
          <h4 className={classes.FavoriteListEntry__title}>{movie.title}</h4>
          <p className={classes.FavoriteListEntry__tagline}>{movie.tagline}</p>
        </div>
        <button onClick={this.handleRemove} className={classes['FavoriteListEntry__remove-button']}>
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
        </button>
      </li>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FavoriteListEntry));
