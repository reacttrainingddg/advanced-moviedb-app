import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import FavoriteListEntry from '../FavoriteListEntry';
import { selectors } from '../../Redux/Store';
import classes from './FavoritesList.module.scss';

const mapStateToProps = (state) => ({
  favorites: selectors.Data.Favorites.favoritesIds(state),
});

class FavoritesList extends React.PureComponent {
  static propsTypes = {
    favorites: PropTypes.arrayOf(PropTypes.number).isRequired,
  };

  render() {
    const { favorites } = this.props;
    return favorites.length > 0 ? (
      <ul className={classes.FavoritesList}>
        {this.props.favorites.map((movieId) => <FavoriteListEntry key={movieId} id={movieId} />)}
      </ul>
    ) : (
      <p className={classes['FavoritesList__empty-msg']}>Mark movies as favorite to see them listed here.</p>
    );
  }
}

export default connect(mapStateToProps)(FavoritesList);
