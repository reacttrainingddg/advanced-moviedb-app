import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import App from './Components/App';
import * as serviceWorker from './serviceWorker';
import configureStore from './Redux/configureStore';

import './Style/index.css';
import {actions} from './Redux/Store';

// create store
const store = configureStore();

const initialMovieId = JSON.parse(localStorage.getItem('currentMovie') || 157336);
store.dispatch(actions.Data.Movies.fetchMovieData(initialMovieId));
store.dispatch(actions.Data.Movies.setCurrentMovie(initialMovieId));

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
