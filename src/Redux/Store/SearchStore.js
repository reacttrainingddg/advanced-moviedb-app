import { createAction } from '@sandstormmedia/react-redux-ts-utils';
import { createSelector } from 'reselect';
import { combineEpics } from 'redux-observable';
import { concat, of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import {filter, mergeMap, switchMap, catchError, delay} from 'rxjs/operators';
import * as Data from './Data';
import buildMovieDBUri from '../../Domain/Service/MovieDBUriService';

//
// State
//
const initialState = {
  isSearching: false,
  errorMessage: ''
};

//
// Actions
//
export const ActionTypes = {
  EXECUTE_SEARCH: '@@MovieDB/Search/Search/EXECUTE_SEARCH',
  SET_SEARCHING: '@@MovieDB/Search/Search/SET_SEARCHING',
  SET_ERROR: '@@MovieDB/Search/Error/SET_ERROR',
};

export const actions = {
  execute: (searchText) => createAction(ActionTypes.EXECUTE_SEARCH, { searchText }),
  setSearching: (isSearching) => createAction(ActionTypes.SET_SEARCHING, { isSearching }),
  setError: (errorMessage) => createAction(ActionTypes.SET_ERROR, { errorMessage }),
};

//
// Reducer
//
export function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.EXECUTE_SEARCH: {
      return {
        ...state,
        isSearching: true,
      };
    }
    case ActionTypes.SET_SEARCHING: {
      return {
        ...state,
        isSearching: action.payload.isSearching,
      };
    }
    case ActionTypes.SET_ERROR: {
      return {
        ...state,
        isSearching: false,
        errorMessage: action.payload.errorMessage,
      };
    }

    default:
      return state;
  }
}

//
// Selectors
//
const isSearching = createSelector(
  [
    (state) => state.Search.isSearching,
  ],
  (isSearching) => isSearching
);

export const selectors = {
  isSearching,
};

//
// Async action handlers (Epics, Sagas, etc)
//
const fetchSearchResultsEpic = (actions$) => actions$.pipe(
  filter((action) => action.type === ActionTypes.EXECUTE_SEARCH),
  mergeMap((action) =>
    ajax(buildMovieDBUri(`/search/movie?language=en-US&query=${encodeURIComponent(action.payload.searchText)}`))
    .pipe(
      switchMap(({ response }) => concat(
        of(actions.setSearching(false)),
        of(Data.actions.Movies.setSearchResults(response))
      )),
      catchError(error => concat(
        of(actions.setError(error.message)),
        of(actions.setError('')).pipe(delay(3000))
      ))
    )
  ),
);

export const epics = combineEpics(
  fetchSearchResultsEpic,
);

/* API Example
{
    "page": 1,
    "total_results": 988,
    "total_pages": 50,
    "results": [
        {
            "vote_count": 202,
            "id": 9090,
            "video": false,
            "vote_average": 6.8,
            "title": "To Wong Foo, Thanks for Everything! Julie Newmar",
            "popularity": 9.592,
            "poster_path": "/xIDEoG9FQGmMCh5XsbkvSuD8WrW.jpg",
            "original_language": "en",
            "original_title": "To Wong Foo, Thanks for Everything! Julie Newmar",
            "genre_ids": [
                35
            ],
            "backdrop_path": "/cDl271jLuysm3Oe6LAzpQ2ZVHw5.jpg",
            "adult": false,
            "overview": "Three New York drag queens on their way to Hollywood for a beauty pageant get stranded in a small Midwestern town for the entire weekend when their car breaks down. While waiting for parts for their Cadillac convertible, the flamboyant trio shows the local homophobic rednecks that appearing different doesn't mean they don't have humanity in common.",
            "release_date": "1995-09-07"
        },
        {
            "vote_count": 48,
            "id": 61488,
            "video": false,
            "vote_average": 7.7,
            "title": "Foo Fighters: Back and Forth",
            "popularity": 2.906,
            "poster_path": "/s0gtjFClqcydcLrLxTeSeLM0Tw9.jpg",
            "original_language": "en",
            "original_title": "Foo Fighters: Back and Forth",
            "genre_ids": [
                10402,
                99
            ],
            "backdrop_path": "/460wZv5WotUSa9PVjmd6RVlPaWK.jpg",
            "adult": false,
            "overview": "FOO FIGHTERS BACK AND FORTH chronicles the 16 year history of the Foo Fighters: from the band's very first songs created as cassette demos Dave Grohl recorded during his tenure as Nirvana's drummer, through its ascent to their Grammy-winning, multi-platinum, arena and stadium headlining status as one of the biggest rock bands on the planet.",
            "release_date": "2011-04-05"
        },
        {
            "vote_count": 5,
            "id": 62944,
            "video": false,
            "vote_average": 6.9,
            "title": "Foo Fighters: Hyde Park",
            "popularity": 1.4,
            "poster_path": "/un93puxVOu3DA4ePAXAfUEr1TRR.jpg",
            "original_language": "en",
            "original_title": "Foo Fighters: Hyde Park",
            "genre_ids": [
                10402
            ],
            "backdrop_path": "/qBQlhYaFvuldQuJuuI4NCCP5MV7.jpg",
            "adult": false,
            "overview": "Recorded at Hyde Park in London, June 17th 2006, this 76 minute performance sees the Foo Fighters rock through some of their most popular material in front of a massive crowd, quoted in some media as nearly 100,000 people. At the time of this show, the band were touring in support of their excellent double album, In Your Honor. There have been a number of different members come and go in this band, however their current line-up has been the most stable.",
            "release_date": "2006-12-04"
        },
        {
            "vote_count": 13,
            "id": 22258,
            "video": false,
            "vote_average": 8.3,
            "title": "Foo Fighters: Live at Wembley Stadium",
            "popularity": 1.175,
            "poster_path": "/c6GaS13FW15oQwf0VFMZ2V59Zcq.jpg",
            "original_language": "en",
            "original_title": "Foo Fighters: Live at Wembley Stadium",
            "genre_ids": [
                10402
            ],
            "backdrop_path": "/pX6H4Yon81nxh14ZnVQDixO66eK.jpg",
            "adult": false,
            "overview": "Foo Fighters captured over their two sold-out nights at Wembley on June 6th and 7th 2008.",
            "release_date": "2008-08-25"
        },
        {
            "vote_count": 8,
            "id": 21114,
            "video": false,
            "vote_average": 8,
            "title": "Foo Fighters: Skin and Bones",
            "popularity": 1.336,
            "poster_path": "/AaOvvlSbWSneLtgt0jwvvUDMFn9.jpg",
            "original_language": "en",
            "original_title": "Foo Fighters: Skin and Bones",
            "genre_ids": [
                10402
            ],
            "backdrop_path": "/9a4pnwvP9ChQlxRbY0bEXBxHAvt.jpg",
            "adult": false,
            "overview": "Skin and Bones is a live acoustic album by the Foo Fighters released on November 7, 2006. The 15-track set was recorded in late August 2006 at the Pantages Theater in Los Angeles and spotlights an expanded eight-piece lineup featuring violinist/singer Petra Haden, former Germs/Nirvana/Foo Fighters guitarist Pat Smear, Wallflowers keyboardist Rami Jaffee, and percussionist Drew Hester.",
            "release_date": "2006-11-28"
        },
        {
            "vote_count": 0,
            "id": 339651,
            "video": false,
            "vote_average": 0,
            "title": "Little Rabbit Foo Foo",
            "popularity": 0.6,
            "poster_path": "/jWpM3217PPUbufTH7GNOa8QiCvc.jpg",
            "original_language": "en",
            "original_title": "Little Rabbit Foo Foo",
            "genre_ids": [
                10751,
                16
            ],
            "backdrop_path": "/t2Ik0kuaPAZJTaGECJaKWkEMy3k.jpg",
            "adult": false,
            "overview": "He's wild, he's wicked, he's Little Rabbit Foo Foo! The bully-boy bunny likes nothing better than to ride through the forest bopping everyone on the head. Wriggly worms, tigers, no one is safe. But here comes the Good Fairy – and she is not amused!",
            "release_date": "2009-09-07"
        },
        {
            "vote_count": 3,
            "id": 281921,
            "video": false,
            "vote_average": 5.2,
            "title": "Foo-Foo Dust",
            "popularity": 0.682,
            "poster_path": "/jMLtEFhRuK1OwYaJ1F3eBWn2EvN.jpg",
            "original_language": "en",
            "original_title": "Foo-Foo Dust",
            "genre_ids": [
                99
            ],
            "backdrop_path": "/2WHLrBf2iGYTG5JyANH0iLUwklj.jpg",
            "adult": false,
            "overview": "This less-than-feature-length documentary chronicles the endless cycle of addiction perpetrated by a mother and son living in a squalid tenement in San Francisco. 22-year-old Ryan and his mother Stephanie are both drug addicts: Although he'll take whatever comes along, her substance of choice is crack cocaine, and she demands that her son provide her with some. As they navigate their respective addictions, each comes close to overdosing just before they're evicted from their apartment.",
            "release_date": "2003-06-19"
        },
        {
            "vote_count": 0,
            "id": 569360,
            "video": false,
            "vote_average": 0,
            "title": "Cum Foo",
            "popularity": 0.6,
            "poster_path": null,
            "original_language": "en",
            "original_title": "Cum Foo",
            "genre_ids": [],
            "backdrop_path": null,
            "adult": false,
            "overview": "A punk spoof with music by Georg Deitl, lyrics by Robert Huot, performed by George A and The Super Connie featuring Bobby Beethoven. Plus electronic music by Brendt Conrad. Starring the beautiful Ama Zeena and sinister Dick Darth.",
            "release_date": "1981-01-01"
        },
        {
            "vote_count": 1,
            "id": 466096,
            "video": true,
            "vote_average": 1,
            "title": "Foo Fighters at Glastonbury 2017",
            "popularity": 0.6,
            "poster_path": "/2HHjYE7wtZD6lwmLRPZ01djfJ61.jpg",
            "original_language": "en",
            "original_title": "Foo Fighters at Glastonbury 2017",
            "genre_ids": [],
            "backdrop_path": "/rBSqrvj7SlM8g0MpK2NfoeT3xy9.jpg",
            "adult": false,
            "overview": "Foo Fighters at Glastonbury 2017",
            "release_date": "2017-06-23"
        },
        {
            "vote_count": 0,
            "id": 137017,
            "video": true,
            "vote_average": 0,
            "title": "Foo Fighters - Global Citizen Festival",
            "popularity": 0.846,
            "poster_path": "/yUh3SACq6X1ehjqSu2QvwojaSPx.jpg",
            "original_language": "en",
            "original_title": "Foo Fighters - Global Citizen Festival",
            "genre_ids": [
                10402
            ],
            "backdrop_path": "/yf1Hnq40JBOUiyBuanXx1ODxpEL.jpg",
            "adult": false,
            "overview": "September 29, 2012 - Palladia Broadcast 1080i  Foo Fighters  Global Citizen Festival 2012  Central Park Great Lawn  New York, NY  September 29, 2012  Palladia broadcast  Lineage: digital cable &gt; Firewire &gt; MacBook Pro &gt; VideoReDo TV Suite 3 &gt; .mpeg  Sequence Summary:  File Size Processed: 4.19 GB, Play Time: 00h:51m:03s\r 1920 x 1080, 29.97 fps, 20.00 Mbps (11.19 Mbps Average).\r Average Video Quality: 45.57 KB/Frame, 0.18 Bits/Pixel.\r AC3 Audio: 2/0 Channels (L, R), 48.0 kHz, 384 kbps.\r Dialog Normalization: -24.0 dB\r 0 of 91823 video frames found with errors.\r 0 of 95744 audio frames found with errors",
            "release_date": "2012-09-29"
        },
        {
            "vote_count": 14,
            "id": 48943,
            "video": false,
            "vote_average": 6.4,
            "title": "Young and Dangerous 5",
            "popularity": 3.629,
            "poster_path": "/77HGmxZEwsZuHVwkpNJGCzCZTts.jpg",
            "original_language": "cn",
            "original_title": "98古惑仔之龍爭虎鬥",
            "genre_ids": [
                28,
                80,
                53
            ],
            "backdrop_path": "/mWBh5GEk1x2xbSEAMKYId5LL5F5.jpg",
            "adult": false,
            "overview": "Although Chicken does not make an appearance, Chan Ho Nam finds a new love interest in the form of Mei Ling (Shu Qi). Meanwhile, Tung Sing returns to cause trouble again for Hung Hing, in the form of new leader Szeto Ho Nam (Mark Cheng). Young and Dangerous 5 occurs during the transfer of Hong Kong sovereignty to China, with the \"boys\" becoming \"men\", as they develop and mature into more business-like dealings.",
            "release_date": "1998-01-01"
        },
        {
            "vote_count": 0,
            "id": 70502,
            "video": true,
            "vote_average": 0,
            "title": "Foo Fighters LIVE on Sydney Harbour",
            "popularity": 0.965,
            "poster_path": "/dDkVCenCHyY3mQ9zkbGlwq8Z2w7.jpg",
            "original_language": "en",
            "original_title": "Foo Fighters LIVE on Sydney Harbour",
            "genre_ids": [
                10402
            ],
            "backdrop_path": "/eehYqx3LSlablKeY7t93QtGYEmA.jpg",
            "adult": false,
            "overview": "In an Australian first, fans will be treated to a live performance of songs from the rock powerhouse's highly anticipated new album, Wasting Light, ahead of its release on April 8. Not only that, it will be the first time that their new material is televised and broadcast.",
            "release_date": "2011-04-02"
        },
        {
            "vote_count": 0,
            "id": 582758,
            "video": true,
            "vote_average": 0,
            "title": "Foo Fighters: VH1 Storytellers",
            "popularity": 0.6,
            "poster_path": "/lRZb7GT2yPTCL7kqCUEUezkBNxe.jpg",
            "original_language": "en",
            "original_title": "Foo Fighters: VH1 Storytellers",
            "genre_ids": [],
            "backdrop_path": "/lMnlyd8WGK7csCZxYZ3rJGDBLdz.jpg",
            "adult": false,
            "overview": "Broadcast recording of Foo Fighters, recorded at the MTV Studios, in Los Angeles, CA on October 28th, 2009.  DVD has a total running time of 43 minutes. Track listing is as follows:  This is a call\r Big me\r Wheels\r My hero\r Word Forward\r Everlong",
            "release_date": "2009-06-10"
        },
        {
            "vote_count": 1,
            "id": 77760,
            "video": true,
            "vote_average": 9,
            "title": "Foo Fighters: iTunes Festival",
            "popularity": 0.6,
            "poster_path": "/2LYtHsBMPelIpG5yWO9t1IAFoC8.jpg",
            "original_language": "en",
            "original_title": "Foo Fighters: iTunes Festival",
            "genre_ids": [
                10402
            ],
            "backdrop_path": "/jjBfAZN6VGjbGcWatFdGXAFVG2s.jpg",
            "adult": false,
            "overview": "iTunes Festival Roundhouse, London July 11, 2011 156.19 Minutes Tracks 01. Bridge Burning 02. Rope 03. The Pretender 04. My Hero 05. Learn to Fly 06. White Limo 07. Arlandria 08. Breakout 09. Cold Day in the Sun 10. Long Road to Ruin 11. Stacked Actors 12. Walk 13. Dear Rosemary 14. Monkey Wrench 15. Let It Die 16. These Days 17. Generator 18. Shake Your Blood * 19. Best of You 20. Skin and Bones ** 21. All My Life Encore: 22. Wheels 23. Times Like These 24. Young Man Blues 25. Miss The Misery 26. Tie Your Mother Down *** 27. Everlong * with Lemmy Kilmister ** infamous incident *** Queen cover with Queen members",
            "release_date": "2011-07-11"
        },
        {
            "vote_count": 0,
            "id": 105354,
            "video": false,
            "vote_average": 0,
            "title": "Foo Fighters: Guardian Angels",
            "popularity": 0.6,
            "poster_path": "/oy9SAE8ehCSAs1SxmbARZQVg9sv.jpg",
            "original_language": "en",
            "original_title": "Foo Fighters: Guardian Angels",
            "genre_ids": [
                99
            ],
            "backdrop_path": null,
            "adult": false,
            "overview": "Attempting to pick up where Nirvana left off, Dave Grohl and his everchanging line up of FooFighters were doomed to failure upon the release of their debut in 1995.It is therefore with respect due, that 10 years on the band are ready to release their fifth album, and the signs are that it could be their best yet. With four classics under their belt and their popularity always heading upwards with each new release, this could well be the Foos finest summer to date! With numerous festival appearances scheduled and a European tour to follow, the time couldn’t be better for fans to go behind the scenes and get the full low down on this extraordinary band.They can do just that with Foo Fighters:Guardian Angels, the first ever Foo Fighters documentary DVD.Going right back to Grohl’s pre Nirvana punk roots, with previously unseen concert footage from that time and coming right up to date with the recording sessions for ‘In Your Honor’ this is one film that no Foos fan will want to be without.",
            "release_date": "2005-06-21"
        },
        {
            "vote_count": 1,
            "id": 195599,
            "video": false,
            "vote_average": 6,
            "title": "Ching Ling Foo Outdone",
            "popularity": 0.6,
            "poster_path": null,
            "original_language": "en",
            "original_title": "Ching Ling Foo Outdone",
            "genre_ids": [],
            "backdrop_path": null,
            "adult": false,
            "overview": "The magician enters upon a stage, and removing a covering from a small table, shakes it before the audience to show that there is nothing whatever concealed inside, places the cloth upon the floor, and when he removes it five large geese are found to be swimming in the water. The magician takes the geese out of the tub, and places them on the stage, and they walk away. He again places the cloth over the tub, and when he removes it the tub disappears and a small boy stands in its place.",
            "release_date": "1900-02-21"
        },
        {
            "vote_count": 0,
            "id": 217223,
            "video": true,
            "vote_average": 0,
            "title": "Foo Fighters: Greatest Hits",
            "popularity": 0.6,
            "poster_path": "/sVxHk5aUGmaw2n78m8K79vSeJUk.jpg",
            "original_language": "en",
            "original_title": "Foo Fighters: Greatest Hits",
            "genre_ids": [
                10402
            ],
            "backdrop_path": "/2Hw1CwVp5UcELAfrYKRIGCWe3oJ.jpg",
            "adult": false,
            "overview": "2009 collection from Dave Grohl and the Foos including two brand new tracks, 'Wheels' and 'Word Forward', both recorded with producer Butch Vig (who also produced Grohl's former band, Nirvana!). This excellent collection features some of the finest melodic Rock released in the last 15 years including 'The Pretender', 'All My Life', 'Learn To Fly', 'Best of You', 'Times Like These', 'My Hero', 'Everlong', and many more.",
            "release_date": "2009-11-02"
        },
        {
            "vote_count": 0,
            "id": 503034,
            "video": false,
            "vote_average": 0,
            "title": "Lick Library Jam With Foo Fighters",
            "popularity": 0.6,
            "poster_path": "/A2QOGbwlhd9ENMOFRFgZFLjAuPz.jpg",
            "original_language": "en",
            "original_title": "Lick Library: Jam With Foo Fighters",
            "genre_ids": [
                10402
            ],
            "backdrop_path": null,
            "adult": false,
            "overview": "Includes track tutorials and Guitar jam tracks, taught by Danny Gill. This excellent DVD will show you how to nail seven tracks by these alternative rock giants whose Guitar driven songs are at the forefront of contemporary rock! Learn each song and play along with Guitar jam tracks. Danny Gill is a former pupil of Joe Satriani, and co-author of the Musicians Institute Rock Lead Guitar series. His songs have appeared on numerous TV shows including 'The Osbournes' as well as motion picture soundtracks such as 'Insomnia' and 'Under Siege'.Includes these songs:All My LifeDOAEverlongMonkey WrenchThe Best Of YouThe PretenderTimes Like These",
            "release_date": "2000-01-01"
        },
        {
            "vote_count": 0,
            "id": 134815,
            "video": true,
            "vote_average": 0,
            "title": "Foo Fighters - Reading Festival",
            "popularity": 0.67,
            "poster_path": "/lykyCu18aDLFIOPK5KSPPEOkbgg.jpg",
            "original_language": "en",
            "original_title": "Foo Fighters - Reading Festival",
            "genre_ids": [
                10402
            ],
            "backdrop_path": "/5e8rV7j1WfCEERBmWZl8GrxpifB.jpg",
            "adult": false,
            "overview": "Foo Fighters Reading Festival Little John's Farm Reading , UK  26th August 2012  Video - PAL 720x576 16:9 25fps Audio - MP2 256 Kbps  01. Intro 02. White Limo 03. All My Life 04. Rope 05. The Pretender 06. My Hero 07. Learn to Fly 08. Arlandria 09. Breakout 10. Cold Day in the Sun 11. I'll Stick Around 12. Generator 13. These Days 14. Monkey Wrench 15. Hey, Johnny Park! 16. Alone and Easy Target 17. Bridge Burning 18. This is a Call 19. In the Flesh 20. Best of You 21. Dave Talks 22. Times Like These 23. Winnebago 24. Watershed 25. For All the Cows 26. Exhausted 27. Everlong  Runnin Time - 2hrs 20m",
            "release_date": "2012-08-26"
        },
        {
            "vote_count": 1,
            "id": 155488,
            "video": true,
            "vote_average": 8,
            "title": "Foo Fighters - Live T In The Park",
            "popularity": 0.625,
            "poster_path": "/pxwRx4EvrLAep8JGUKWLI5S3455.jpg",
            "original_language": "en",
            "original_title": "Foo Fighters -T in The Park",
            "genre_ids": [
                10402
            ],
            "backdrop_path": "/qhRRRpn897K1TVm6oTA30LTrcNj.jpg",
            "adult": false,
            "overview": "The Foo Fighter's incredible headlining performance at the 2011 T in The Park Festival is now available on DVD, exclusively at DVDRare.com! This DVD contains every second of their lengthy set at the legendary Scottish music festival in high video and audio quality!  Main Stage  Balado Station, Scotland  July 10, 2011  Setlist:  Bridge Burning  Rope  The Pretender  My Hero  Learn to Fly  White Limo  Arlandria  Breakout  Long Road to Ruin  Cold Day in the Sun  Stacked Actors  Walk  Monkey Wrench  Let It Die  These Days  Best of You  All My Life  Wheels  Times Like These  Young Man Blues  This is a Call  Everlong",
            "release_date": "2011-07-09"
        }
    ]
}
*/
