import { actions, reducer }from './FavoritesStore';

describe("Testing of a reducer", () => {

  const initialState = {
    favoritesIds: []
  };

  it("correctly adds a favorite id", () => {
    expect(reducer(initialState, actions.addFavorite(1234))).toEqual({
      favoritesIds: [1234]
    });
  });
});
