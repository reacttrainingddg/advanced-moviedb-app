import { combineReducers } from 'redux';
import { combineEpics } from 'redux-observable';

import * as MovieStore from './MoviesStore'
import * as FavoritesStore from './FavoritesStore'

export const actions = {
  Movies: MovieStore.actions,
  Favorites: FavoritesStore.actions,
};

export const selectors = {
  Movies: MovieStore.selectors,
  Favorites: FavoritesStore.selectors,
};

export const epics = combineEpics(
  MovieStore.epics,
);

export const reducers = combineReducers({
  Movies: MovieStore.reducer,
  Favorites: FavoritesStore.reducer,
});
