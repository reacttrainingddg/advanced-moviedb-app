import { createAction } from '@sandstormmedia/react-redux-ts-utils';
import { createSelector } from 'reselect';

// WORKSHOP TASK: sort list by different criteria

// TODO move local storage into own file in src/Domain
const LOCAL_STORAGE_KEY = 'Favorites';

function getFavoritesFromLocalStorage() {
  return JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)) || [];
}

function saveFavoritesToLocalStorage(favorites) {
  localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(favorites));
}

//
// State
//
const initialState = {
  favoritesIds: getFavoritesFromLocalStorage(), // list of numbers
};

//
// Actions
//
export const ActionTypes = {
  ADD_FAVORITE: '@@NameSpace/Data/Favorites/ADD_FAVORITE',
  REMOVE_FAVORITE: '@@NameSpace/Data/Favorites/REMOVE_FAVORITE',
};

export const actions = {
  addFavorite: (movieId) => createAction(ActionTypes.ADD_FAVORITE, { movieId }),
  removeFavorite: (movieId) => createAction(ActionTypes.REMOVE_FAVORITE, { movieId }),
};

//
// Reducer
//
export function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.ADD_FAVORITE: {
      const newFavorites = [...state.favoritesIds, action.payload.movieId];

      saveFavoritesToLocalStorage(newFavorites);

      return {
        ...state,
        favoritesIds: newFavorites,
      };
    }
    case ActionTypes.REMOVE_FAVORITE: {
      const newFavorites = state.favoritesIds.filter(id => id !== action.payload.movieId);

      saveFavoritesToLocalStorage(newFavorites);

      return {
        ...state,
        favoritesIds: newFavorites,
      };
    }
    default:
      return state;
  }
}

//
// Selectors
//
const favoritesIds = (state) => state.Data.Favorites.favoritesIds;

const isMovieFavorite = createSelector(
  (_, movie) => movie,
  favoritesIds,
  (movie, favoritesIds) => movie !== undefined ? favoritesIds.some(id => id === movie.id): false
)

export const selectors = {
  favoritesIds,
  isMovieFavorite,
};
