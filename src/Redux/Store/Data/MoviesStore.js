import { createAction } from '@sandstormmedia/react-redux-ts-utils';
import { createSelector } from 'reselect';
import { combineEpics } from 'redux-observable';
import { ajax } from 'rxjs/ajax';
import { filter, map, mergeMap } from 'rxjs/operators';
import { get } from 'immutable';
import buildMovieDBUri from '../../../Domain/Service/MovieDBUriService';

// TODO move local storage into own file in src/Domain
const getMoviesFromLocalStorage = () => JSON.parse(localStorage.getItem('movies')) || {};
const saveMoviesToLocalStorage = (movies) => {
  localStorage.setItem('movies', JSON.stringify(movies));
};
const getMovieIdsFromLocalStorage = () => JSON.parse(localStorage.getItem('movieIds')) || [];
const saveMovieIdsToLocalStorage = (movieIds) => {
  localStorage.setItem('movieIds', JSON.stringify(movieIds));
}
const getCurrentMovieFromLocalStorage = () => JSON.parse(localStorage.getItem('currentMovie')) || 157336;
const saveCurrentMovieToLocalStorage = (currentMovie) => {
  localStorage.setItem('currentMovie', JSON.stringify(currentMovie));
}

//
// State
//
const initialState = {
  moviesById: getMoviesFromLocalStorage(),
  allMovieIds: getMovieIdsFromLocalStorage(),
  currentMovieId: getCurrentMovieFromLocalStorage(),
  searchResults: [],
};

//
// Actions
//
export const ActionTypes = {
  FETCH_MOVIE_DATA: '@@MovieDB/Data/Movie/FETCH_MOVIE_DATA',
  SET_MOVIE_DATA: '@@MovieDB/Data/Movie/SET_MOVIE_DATA',
  SET_CURRENT_MOVIE: '@@MovieDB/Data/Movie/SET_CURRENT_MOVIE',
  SET_SEARCH_RESULTS: '@@MovieDB/Data/Movie/SET_SEARCH_RESULTS',
};

export const actions = {
  fetchMovieData: (movieId) => createAction(ActionTypes.FETCH_MOVIE_DATA, { movieId }),
  setMovieData: (movie) => createAction(ActionTypes.SET_MOVIE_DATA, { movie }),
  setCurrentMovie: (movieId) => createAction(ActionTypes.SET_CURRENT_MOVIE, { movieId }),
  setSearchResults: (searchResults) => createAction(ActionTypes.SET_SEARCH_RESULTS, { searchResults }),
};

//
// Reducer
//
export function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.SET_MOVIE_DATA: {
      const movie = {...action.payload.movie};
      const allMovieIds = state.allMovieIds.some(id => id === movie.id) ? state.allMovieIds : [...state.allMovieIds, movie.id];
      const moviesById = {
        ...state.moviesById,
        [movie.id]: {
          ...movie,
          backdrop_path: `https://image.tmdb.org/t/p/original${movie.backdrop_path}`,
          poster_path: `https://image.tmdb.org/t/p/w300${movie.poster_path}`,
        },
      };

      saveMoviesToLocalStorage(moviesById);
      saveMovieIdsToLocalStorage(allMovieIds);

      return {
        ...state,
        allMovieIds,
        moviesById,
        searchResults: []
      };
    }
    case ActionTypes.SET_CURRENT_MOVIE:
      saveCurrentMovieToLocalStorage(action.payload.movieId);
      return {
        ...state,
        searchResults: [],
        currentMovieId: action.payload.movieId,
      };
    case ActionTypes.SET_SEARCH_RESULTS: {
      const payload = {...action.payload.searchResults};

      return {
        ...state,
        searchResults: payload.results,
      };
    }
    default:
      return state;
  }
}

//
// Selectors
//
const moviesByIdSelector = (state) => state.Data.Movies.moviesById;

const currentMovie = createSelector(
  [
    (state) => state.Data.Movies.currentMovieId,
    moviesByIdSelector,
  ],
  (currentMovieId, moviesById) => get(moviesById, currentMovieId, undefined)
);

const movieById = createSelector(
  [
    (_, id) => id,
    moviesByIdSelector,
  ],
  (id, moviesById) => moviesById ? get(moviesById, id, undefined) : undefined
);

const searchResults = createSelector(
  [
    (state) => state.Data.Movies.searchResults,
  ],
  (searchResults) => searchResults
);

export const selectors = {
  currentMovie,
  searchResults,
  movieById,
};

//
// Async action handlers (Epics, Sagas, etc)
//
const fetchMovieEpic = (actions$, state$) => actions$.pipe(
  filter((action) => action.type === ActionTypes.FETCH_MOVIE_DATA),
  // abort if movie already loaded
  filter((action) => state$.value.Data.Movies.moviesById[action.payload.movieId] === undefined),
  mergeMap((action) => ajax(buildMovieDBUri(`/movie/${action.payload.movieId}`))
    .pipe(
      map(({ response }) => actions.setMovieData(response))
    ),
  ),
);

export const epics = combineEpics(
  fetchMovieEpic,
);

/* API Example
{
  "adult": false,
  "backdrop_path": "/52AfXWuXCHn3UjD17rBruA9f5qb.jpg",
  "belongs_to_collection": null,
  "budget": 63000000,
  "genres": [
    {
      "id": 18,
      "name": "Drama"
    }
  ],
  "homepage": "http://www.foxmovies.com/movies/fight-club",
  "id": 550,
  "imdb_id": "tt0137523",
  "original_language": "en",
  "original_title": "Fight Club",
  "overview": "A ticking-time-bomb insomniac and a slippery soap salesman channel primal male aggression into a shocking new form of therapy. Their concept catches on, with underground \"fight clubs\" forming in every town, until an eccentric gets in the way and ignites an out-of-control spiral toward oblivion.",
  "popularity": 22.893,
  "poster_path": "/adw6Lq9FiC9zjYEpOqfq03ituwp.jpg",
  "production_companies": [
    {
      "id": 508,
      "logo_path": "/7PzJdsLGlR7oW4J0J5Xcd0pHGRg.png",
      "name": "Regency Enterprises",
      "origin_country": "US"
    },
    {
      "id": 711,
      "logo_path": "/tEiIH5QesdheJmDAqQwvtN60727.png",
      "name": "Fox 2000 Pictures",
      "origin_country": "US"
    },
    {
      "id": 20555,
      "logo_path": null,
      "name": "Taurus Film",
      "origin_country": ""
    },
    {
      "id": 54051,
      "logo_path": null,
      "name": "Atman Entertainment",
      "origin_country": ""
    },
    {
      "id": 54052,
      "logo_path": null,
      "name": "Knickerbocker Films",
      "origin_country": "US"
    },
    {
      "id": 25,
      "logo_path": "/qZCc1lty5FzX30aOCVRBLzaVmcp.png",
      "name": "20th Century Fox",
      "origin_country": "US"
    },
    {
      "id": 4700,
      "logo_path": "/A32wmjrs9Psf4zw0uaixF0GXfxq.png",
      "name": "The Linson Company",
      "origin_country": ""
    }
  ],
  "production_countries": [
    {
      "iso_3166_1": "DE",
      "name": "Germany"
    },
    {
      "iso_3166_1": "US",
      "name": "United States of America"
    }
  ],
  "release_date": "1999-10-15",
  "revenue": 100853753,
  "runtime": 139,
  "spoken_languages": [
    {
      "iso_639_1": "en",
      "name": "English"
    }
  ],
  "status": "Released",
  "tagline": "Mischief. Mayhem. Soap.",
  "title": "Fight Club",
  "video": false,
  "vote_average": 8.4,
  "vote_count": 15664
}
*/
