import { combineReducers } from 'redux';
import { combineEpics } from 'redux-observable';

import * as Data from './Data'
import * as SearchStore from './SearchStore'

export const actions = {
  Data: Data.actions,
  Search: SearchStore.actions,
};

export const selectors = {
  Data: Data.selectors,
  Search: SearchStore.selectors,
};

export const epics = combineEpics(
  Data.epics,
  SearchStore.epics,
);

export const reducers = combineReducers({
  Data: Data.reducers,
  Search: SearchStore.reducer,
});
