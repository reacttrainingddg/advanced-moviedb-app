import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createEpicMiddleware } from 'redux-observable';

import { epics, reducers } from './Store';

export default function configureStore(initialState) {
  // create middlewares
  const epicMiddleware = createEpicMiddleware();

  // create store
  const store = createStore(
    reducers,
    initialState || {},
    composeWithDevTools(
      applyMiddleware(epicMiddleware)
    )
  );

  // start middlewares
  epicMiddleware.run(epics);

  return store;
}
